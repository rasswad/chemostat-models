from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

from chemostat.growth import Monod, ShiftedMonod
from chemostat.model import ABModel 

if __name__ == "__main__":
    ab = ABModel(Monod(1, 0.2), Monod(1, 0.2), ShiftedMonod(1, 0.5, 0.5), 0.8, 2, 1, 1, quota='total')
    c = ab.c_model.reduce_model('z')