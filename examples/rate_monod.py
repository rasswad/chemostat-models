from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

import numpy as np
from matplotlib import pyplot as plt

from chemostat.growth import Monod

if __name__ == "__main__":
    # true parameters
    s = np.arange(0, 8, 0.1)
    monod = Monod(1, 0.3)
    mu = monod(s)

    # perturbed parameters
    delta_mu_max = 0.1
    delta_k = 0.05
    pm = np.array([-1, +1])
    mu_grid = []
    for mu_max in monod.max + pm * delta_mu_max:
        for k in monod.k + pm * delta_k:
            mu_grid.append(Monod(mu_max, k)(s))
    mu_grid = np.array(mu_grid)

    # linearized perturbation
    error_mu_max = lambda s: s / (monod.k + s)
    error_k = lambda s: (monod.k + (1-monod.max)*s) * np.power(monod.k + s, -2)
    delta_grid = np.zeros((4, len(s)))
    for i in [0, 1]:
        for j in [0, 1]:
            delta_grid[2 * i + j] = (pm[i] * delta_mu_max * error_mu_max(s)
                                     + pm[j] * delta_k * error_k(s))

    # PLOTS
    plt.rcParams.update({'font.size': 24})
    plt.figure(figsize=(15, 10))
    # plot true
    mu_label = r"\frac{\bar{\mu} s}{k + s}"
    plt.axhline(y=monod.max, linestyle=':', color='black')
    plt.plot([monod.k] * 2, [0, monod.max / 2], ':', color='black')
    plt.plot([0, monod.k], [monod.max / 2] * 2, ':', color='black')
    plt.plot(s, mu, label=f"${mu_label}$")

    # plot perturbed
    plt.fill_between(s, mu_grid.max(axis=0), mu_grid.min(axis=0), alpha=0.4,
                     label=r"$\frac{(\bar{\mu} \pm \Delta\bar{\mu}) s}"
                           r"{(k \pm \Delta k) + s}$")

    lin_label = f"${mu_label}\\pm\\varepsilon_{{\\bar{{\\mu}},k}} (s)$"
    c = 'red'
    plt.plot(s, mu + delta_grid.max(axis=0), '--', color=c)
    plt.plot(s, mu + delta_grid.min(axis=0), '--', color=c, label=lin_label)

    # adjust plots
    plt.suptitle("Perturbations to the Monod growth rate function")
    plt.legend(loc='lower right')
    plt.ylim(ymin=0)
    plt.xlim(s[0], s[-1])
    plt.show()
