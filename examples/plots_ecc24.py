from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import patches

from chemostat.growth import Monod
from chemostat.model import Trajectory, FluoCstYield
from chemostat.observer.model import conservation_KF_sef, bayesian_KF_sef, biomodelEKF


# stored parameters from previous estimations
BETA  = 0.012597007505479754
THETA = 0.028954021272936833
KAPPA = 0.0019570175438238663
EKF_Q = np.diag([0.06697521, 0.00478882, 1e-10])


def estimate_parameters(fluo: FluoCstYield, t, sol, x0, P0, noise_std,
                        scalar_Q, seed=None):
    # convenience
    I1, I3 = np.eye(1), np.eye(3)
    R = I1 * (noise_std ** 2)
    Q1 = I1 * scalar_Q

    # simulation
    data_noise = np.random.default_rng(seed).normal(scale=noise_std, size=len(t))
    y = sol(t)[2] + data_noise

    # instantiate filters
    ckf = conservation_KF_sef(fluo, R=R, Q=Q1, fluo_sigma='prior')
    bkf = bayesian_KF_sef(fluo, R=R)
    ekf = biomodelEKF(fluo, np.array([[0, 0, 1]]), R=R)

    # estimate ckf smoother
    if not BETA:
        print("estimating parameter 'beta' for CKF...")
        beta = ckf.estimate_beta(t, y)
    else:
        print(f"using stored value BETA={BETA}")
        beta = BETA

    # estimate bkf parameters
    r0 = x0[1]
    cov_r0 = 100
    if THETA and KAPPA:
        print(f"using stored values THETA={THETA}, KAPPA={KAPPA}")
        param = THETA, KAPPA
    else:
        theta, kappa = 0.5, 0.5
        print("estimating parameters 'theta' and 'kappa' for BKF...")
        param = bkf.estimate_parameters(x0, P0, t, y, r0, cov_r0,
                                        theta, kappa, bounds=[(0, 1)]*2)

    # estimate ekf Q
    if EKF_Q is None:
        print("estimating optimal Q for EKF...")
        Q3 = np.ones(3) * 0.1
        ekf.estimate_Q(x0, P0, t, y, Q0_diag=Q3, method='Nelder-Mead')
        Q3 = [ekf.Q[i, i] for i in range(len(ekf.Q))]
    else:
        print(f"using stored matrix EKF_Q=\n{EKF_Q}")
        Q3 = EKF_Q

    return beta, param, Q3


def plot_single_run(fluo: FluoCstYield, t, sol, x0, P0, sigma0,
                    beta, theta, kappa, ckf_Q, ekf_Q,
                    noise_std, seed=None, save=False):
    # convenience
    s, e, f = sol.x[0], sol.x[1], sol.x[2]
    fk = sol(t)[2]
    I1, I3 = np.eye(1), np.eye(3)
    R = I1 * (noise_std ** 2)

    # generate data
    data_noise = np.random.default_rng(seed).normal(scale=noise_std, size=len(t))
    y = fk + data_noise

    # instantiate filters
    ckf = conservation_KF_sef(fluo, R=R, Q=I1*ckf_Q, fluo_sigma='post')
    bkf = bayesian_KF_sef(fluo, R=R)
    ekf = biomodelEKF(fluo, np.array([[0, 0, 1]]), R=R, Q=ekf_Q)

    # run conservation KF
    coef = 1./(fluo.a * fluo.Y)
    sig0 = x0[0] + coef*x0[2]
    obs1 = ckf(x0, P0, t, y, sigma0, beta=beta)
    y_hat = obs1.y_hat

    # estimate Bayesian KF
    r0 = x0_hat[1]
    cov_r0 = 100
    obs2 = bkf(x0, P0, t, y, r0, cov_r0, theta, kappa)
    r_hat = obs2.xhat[:, -1]

    # run EKF
    obs3 = ekf(x0, P0, t, y)

    # calculate growth functions
    sigma = fluo.Sin + (sigma0 - fluo.Sin) * np.exp(fluo.D*(t[0] - t))
    s_sig = lambda yk: sigma - coef*yk
    mu_s = fluo.mu(s)
    mu_hat = fluo.mu(s_sig(y_hat))
    r = mu_s * e

    # FIGURE 1: measurements & growth estimations
    fig, (ax1, ax21) = plt.subplots(2, 1, sharex=True,
                                    figsize=(FIG_WIDTH, 0.6 * FIG_WIDTH))

    # plot measurements
    ax1.plot(t, y, '.', color='C2', alpha=0.4, label="$y_k$")
    ax1.plot(t, y_hat, '-', color='C3', label=r"$\widehat{y}_k$")
    ax1.plot(sol.t, f, color='C0', label="$f$")
    #smoothed_std = round(np.std(fk - y_hat), 4)
    #text_y = (f"$\\mathrm{{std}}(y_k - f(t_k))={noise_std}$\n"
    #          f"$\\mathrm{{std}}(\\widehat{{y}}_k - f(t_k))={smoothed_std}$\n"
    #          f"$\\beta = {round(beta, 4)}$")
    #handles, labels = ax1.get_legend_handles_labels()
    #handles.append(patches.Patch(color='none', label=text_y))
    #plt_legend(ax1, handles=handles)
    plt_legend(ax1, bbox_to_anchor=(1, 1.07))
    ax1.set_ylim(0, ax1.get_ylim()[1])
    ax1.set_xlim(t[0], t[-1])
    ax1.set_xticks(np.arange(t[0], t[-1] + 0.1, 6))
    ax1.set_ylabel('$y$ [$gL^{-1}$]')

    # plot growth function
    ax21.plot(sol.t, mu_s, color='C0', label=r"$\mu(s)$")
    ax21.plot(t, mu_hat, '--', color='C0', label=r"$\widehat{\mu}$")
    ax21.set_xlabel('Time [$h$]')
    ax21.set_ylabel('$\mu(s)$ [$h^{-1}$]')
    ax21.set_ylim(0, ax21.get_ylim()[1])
    ax21.set_yticks(np.arange(0, ax21.get_ylim()[1], 0.2))

    # plot reaction rate
    ax22 = ax21.twinx()
    ax22.plot(sol.t, r, color='C2', label=r"$r$")
    ax22.plot(t, r_hat, '--', color='C2', label=r"$\widehat{r}$")
    ax22.set_ylabel('$r$ [$gL^{-1}h^{-1}$]')
    ax22.set_ylim(0, 0.6)

    fig.tight_layout()
    plt.subplots_adjust(hspace=0)
    ax1.set_yticks(np.arange(0, ax1.get_ylim()[1], 0.1))
    if save:
        plt.savefig(f"{SAVE_DIR}/measurement.pdf")

    # FIGURE 2: estimated trajectories
    fig, axs = plt.subplots(3, 1, sharex=True)
    plt.subplots_adjust(hspace=0)
    x1 = obs1.xhat.T
    x2 = obs2.xhat.T
    x3 = obs3.xhat.T
    for i, (v, ax) in enumerate(zip(['s', 'e', 'f'], axs.flatten())):
        ax.plot(t, x3[i], '-', color='C1', label=f"EKF", alpha=0.5)
        ax.plot(t, x1[i], '-', color='C0', label=f"CKF")
        ax.plot(t, x2[i], '-', color='C2', label=f"BKF")
        ax.plot(sol.t, sol.x[i], color='C3', label=f"$x(t)$")
        ax.set_ylim(0, ax.get_ylim()[1])
    axs[2].set_xlabel("Time [$h$]")
    axs[0].set_ylabel("$s$ [$g/L$]")
    axs[1].set_ylabel("$e$ [$g/L$]")
    axs[2].set_ylabel("$f$ [$g/L$]")
    axs[0].set_xlim(t[0], t[-1])
    axs[1].set_xticks(np.arange(t[0], t[-1]+0.1, 6))
    axs[0].set_ylim(0, 1.8)
    axs[0].set_yticks(np.arange(0, 1.9, 0.6))
    axs[1].set_ylim(0, 1.5)
    axs[1].set_yticks(np.arange(0, 1.6, 0.6))
    axs[2].set_yticks(np.arange(0, 0.25, 0.1))
    plt_legend(axs[0], bbox_to_anchor=(1, 1.07))

    fig.tight_layout()
    plt.subplots_adjust(hspace=0)
    if save:
        plt.savefig(f"{SAVE_DIR}/trajectory.pdf")

def set_mpl_global():
    #mpl.use("pgf")
    c = ['0C5DA5', '00B945', 'FF9500', 'FF2C00', '845B97', '474747', '9e9e9e']
    mpl.rcParams.update(
        {
            #"pgf.texsystem": "pdflatex",
            # "text.usetex": True,
            "font.family": "serif",
            "mathtext.fontset": "cm",
            # "pgf.rcfonts": False,
            "figure.figsize": [FIG_WIDTH, FIG_RATIO * FIG_WIDTH],
#            "font.size": 18,
            "axes.prop_cycle": mpl.cycler('color', c),
            "xtick.direction": "in",
            "ytick.direction": "in",
            "axes.linewidth": 0.5,
            "savefig.dpi": 300,
        }
    )

def plt_legend(ax, **kwargs):
    legend = ax.legend(fancybox=False, edgecolor="black", **kwargs)
    legend.get_frame().set_linewidth(0.5)
    return legend

if __name__ == "__main__":
    TEXT_WIDTH = 7
    FIG_WIDTH = 3.4 #8
    FIG_RATIO = 0.75
    SAVE_DIR = "/home/rand/repo/chemostat/plots/2023_11_02"
    set_mpl_global()

    # model parameters
    gamma = 1
    min_doubling_time = 1
    mu_max = np.log(2) / min_doubling_time
    ks = 0.1
    d = 0.008 * 60
    s_in = 2
    alpha = 0.3
    monod = Monod(mu_max, ks)
    fluo = FluoCstYield(monod, d, s_in, gamma, alpha)
    N = np.array([[-1/gamma, 1-alpha, alpha]]).T
    x_in = np.array([[d * s_in, 0, 0]]).T

    # initial conditions
    t = np.arange(0, 36, 1/60)  # 1 sample per minute over 36 hours [h]
    t_interval = 5  # integer time in minutes between measures
    tk = t[::t_interval]
    x0 = np.array([0, 1, 0])
    x0_hat = np.array([0.5, 1.5, 0])
    sigma0 = 0.5
    P0 = np.eye(3) * (1**2)

    # generate trajectory
    sol = fluo.calculate_trajectory(x0, (t[0], t[-1]), t_eval=t)

    # random generator seeds
    parameter_data_seed = 5420627
    filter_data_seed    = 95472802

    # noise std
    y_noise_std = 0.015
    a = monod.derivative(monod.inverse(d/(1-alpha))) / (alpha * gamma)
    Q = (a * y_noise_std) ** 2 * (t[1] - t[0])

    # estimate parameters then save
    beta, (theta, kappa), ekf_Q = estimate_parameters(
        fluo, tk, sol, x0_hat, P0, y_noise_std, Q, seed=parameter_data_seed
    )

    # plot single run results
    plot_single_run(fluo, tk, sol, x0_hat, P0, sigma0, beta, theta, kappa, Q,
                    ekf_Q, y_noise_std, seed=filter_data_seed, save=True)

    # Monti-Carlo plots
    #plot_monti_carlo(fluo, sol, x0_hat, P0, beta, theta, kappa, Q, ekf_Q,
    #                 y_noise_std, seed=filter_data_seed)

    plt.show()
