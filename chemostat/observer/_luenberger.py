import numpy as np

from ._base import Observer
from chemostat.model import Biomodel
from chemostat.utils import sample_and_hold


class LuenbergerLike(Observer):
    def __init__(self, dynamic_fun, measure_matrix=None):
        if isinstance(dynamic_fun, Biomodel):
            self.f = dynamic_fun._get_ode(1)
        elif callable(dynamic_fun):
            self.f = dynamic_fun
        else:
            raise ValueError("'dynamic_fun' must be an instance of 'Biomodel' "
                             "or a callable that takes two arguments (t,x)")
        self.C = measure_matrix
        self.y = None
        self.gain = None

    def measure_fun(self, t, x) -> np.ndarray:
        if isinstance(self.C, (list, np.ndarray)):
            return np.dot(self.C, x)
        if callable(self.C):
            return np.dot(self.C(t), x)
        raise NotImplementedError

    def dynamics_fun(self, t, x, y, L) -> np.ndarray:
        """
        dx̂/dt = f(x̂) + L (y - ŷ)
        """
        f_xhat = self.f(t, x)
        innovation = y - self.measure_fun(t, x)
        return f_xhat + np.dot(L, innovation)

    def _get_ode(self):
        def f(t, x):
            L = self.gain(t) if callable(self.gain) else self.gain
            return self.dynamics_fun(t, x, self.y(t), L)
        return f

    def __call__(self, x0, t, y, gain, **kwargs):
        self.y = sample_and_hold(t, y)
        if isinstance(gain, (list, np.ndarray)):
            gain = np.array(gain)
            if gain.ndim < 3:
                self.gain = gain
            elif gain.ndim == 3:
                self.gain = sample_and_hold(t, gain)
        return Observer.__call__(self, x0, t, y, **kwargs)
