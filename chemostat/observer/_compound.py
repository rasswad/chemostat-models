import warnings

import numpy as np
from scipy.optimize import minimize, minimize_scalar

from ._kalman import KalmanFilter
from chemostat.model import Biomodel


class ConservationKF(KalmanFilter):
    def __init__(self, model: Biomodel, growth_rate, A, sigma, b=0, R=None,
                 Q=None, G=None, fluo_sigma='post'):
        self._sigma = sigma         # (t, t0, σ0) |-> σ(t)
        self.mu = growth_rate       # (σ, y) |-> μ(s)
        self.A_func = A             # μ(s) |-> A
        dim = len(model)
        A = np.array([[0, 1], [0, 0]])
        C = np.array([[1, 0]])
        G1 = np.array([[0, 1.0]]).T
        I1 = np.eye(1)
        self.smoother = KalmanFilter(2, 1, A, C, R=R, Q=I1, G=G1)
        self.smoother.save = False
        A = np.zeros((dim, dim))
        C = np.zeros((1, dim))
        C[0, -1] = 1
        Q = I1 if Q is None else I1 * float(Q)
        KalmanFilter.__init__(self, dim, 1, A, C, b=b, R=R, Q=Q, G=G)
        self.fluo = fluo_sigma.lower() # should assert valid

    def update(self, y, R=None):
        KalmanFilter.update(self, y, R)
        if self.fluo == 'raw':
            y = float(y)
            mu = self.mu(self.sigma(self.t), y)
            self.A = lambda tau: self.A_func(self.mu(self.sigma(tau), y))
        else:
            self.smoother.update(y, R)
            y = float(self.smoother.x[0])
            mu = self.mu(self.sigma(self.t), y)
            if self.fluo == 'post':
                self.A = lambda tau: self.A_func(self.mu(self.sigma(tau), y))
            if self.save:
                self.y_hat[self.k] = y
                self.mu_hat[self.k] = mu

    def predict(self, t, Q=None):
        if self.fluo in ['prior', 'post']:
            self.smoother.predict(t)
            if self.fluo == 'prior':
                y = lambda tau: float(self.smoother.x_prior(tau)[0])
                self.A = lambda tau: self.A_func(self.mu(self.sigma(tau), y(tau)))
        KalmanFilter.predict(self, t)

    def set_fluo_pre(self, fluo):
        if fluo is None or str(fluo).lower() == 'default':
            self.fluo = 'post'
            return self.fluo
        fluo = str(fluo).lower()
        if fluo in ['post', 'prior', 'raw']:
            self.fluo = fluo
            return self.fluo
        raise ValueError("value not in 'post', 'prior', 'raw'")

    def __call__(self, x0, P0, t, y, sigma0=None, beta=None, fluo_pre=None):
        if sigma0 is None:
            warnings.warn("'sigma0' is a mandatory argument, using 0 instead.")
            sigma0 = 0
        fluo_pre = self.set_fluo_pre(fluo_pre)
        if fluo_pre in ['post', 'prior']:
            if beta is None:
                warnings.warn("β parameter missing, using 'raw' output instead")
                self.__call__(x0, P0, t, y, sigma0, fluo_pre='raw')
            self.smoother.G[-1, 0] = beta
            if self.save:
                self.y_hat = np.nan * np.ones(len(t))
                self.mu_hat = np.nan * np.ones(len(t))
        self.sigma = lambda tau: self._sigma(tau, t[0], sigma0)
        e = KalmanFilter.__call__(self, x0, P0, t, y)
        if fluo_pre in ['post', 'prior']:
            e.y_hat = self.y_hat
            e.mu = self.mu_hat
            e.beta = beta
        return e

    def estimate_beta(self, t, y, bounds=None, **kwargs):
        x0 = [y[0], 0]
        P0 = np.diag([float(self.smoother.R), 100])
        def objective_func(val):
            self.smoother.G[-1, 0] = val
            self.smoother.soft_run(x0, P0, t, y)
            return -self.smoother.log_likelihood
        opt = minimize_scalar(objective_func, bounds=bounds, **kwargs)
        if opt.success:
            print(f"Estimated parameter successfully: β={opt.x}")
            beta = opt.x
            self.smoother.G[-1, 0] = beta
        else:
            warnings.warn(f"Parameter optimization failed for β\n"
                          f"Message: '{opt.message}'")
            beta = None
        return beta


class BayesianKF(KalmanFilter):
    def __init__(self, N, d, x_in, C, R=None):
        dim = len(N) + 1
        # A = [[-dI, N], [0, -θ]]
        A = -np.eye(dim)
        A[:-1, :-1] *= d
        A[:-1, -1] = N
        # b = [d x_in, 0].T
        b = np.zeros((dim, 1))
        b[:-1, 0] = d * x_in
        # G = [0,κ].T
        G = np.zeros((dim, 1))
        C = np.array(C).reshape((1, C.size))
        KalmanFilter.__init__(self, dim, 1, A, C, b=b, R=R, Q=np.eye(1), G=G)

    def get_state(self, x, r): return np.append(x, r)

    def get_cov(self, cov_x, cov_r):
        cov = np.eye(self.dim_x)
        cov[:-1, :-1] = cov_x
        cov[-1, -1] = cov_r
        return cov

    def set_parameters(self, params):
        self.A[-1, -1] = -params[0]
        self.G[-1, 0] = params[1]

    def __call__(self, x0, cov_x0, t, y, r0, cov_r0, theta, kappa):
        z0 = self.get_state(x0, r0)
        P0 = self.get_cov(cov_x0, cov_r0)
        self.set_parameters([theta, kappa])
        e = KalmanFilter.__call__(self, z0, P0, t, y)
        e.theta = theta
        e.kappa = kappa
        return e

    def estimate_parameters(self, x0, cov_x0, t, y, r0, cov_r0, theta, kappa, **kwargs):
        z0 = self.get_state(x0, r0)
        P0 = self.get_cov(cov_x0, cov_r0)
        p = np.array([theta, kappa])
        def objective_func(params):
            self.set_parameters(params)
            self.soft_run(z0, P0, t, y)
            return -self.log_likelihood
        opt = minimize(objective_func, p, **kwargs)
        if opt.success:
            p = opt.x
            print(f"Estimated parameters successfully: θ={p[0]}, κ={p[1]}")
        else:
            warnings.warn(f"Parameter estimation failed for (θ,κ)\n"
                          f"Using passed estimates: ({theta},{kappa})\n"
                          f"Message: '{opt.message}'")
        self.set_parameters(p)
        return p[0], p[1]
