import warnings
from sys import float_info

import numpy as np
from scipy.optimize import minimize

from ._base import StateEstimation, Observer
from chemostat.utils import solve_ivp


def log_normal_pdf_kf(y_error: np.ndarray, S, S_inv):
    det_S = np.linalg.det(S)
    if det_S <= float_info.min:
        return np.inf
    return -0.5 * (y_error.size * np.log(2*np.pi) + np.log(det_S)
                   + float(y_error.T @ S_inv @ y_error))


class KalmanFilter(Observer):
    def __init__(self, dim_x, dim_y, A, H, b=0, R=None, Q=None, G=None):
        # should assert coherent dims
        self.dim_x = dim_x
        self.dim_y = dim_y
        self.A = A
        self.H = H
        self.b = b

        # time
        self.k = 0
        self.t = 0
        self.t0 = 0
        self.t1 = 0

        # prediction-correction variables
        self.x = np.zeros((dim_x, 1))
        self.P = np.zeros((dim_x, dim_x))
        self.y = np.zeros((dim_y, 1))

        # measures and gains
        self.K = np.zeros((dim_x, dim_y))

        # save
        self.save = True
        self.x_post = None
        self.P_post = None
        self.K_post = None

        # default covariance matrices
        if G is None:
            if Q is None:
                self.Q = np.eye(dim_x)
                self.G = 1
            else:
                self.Q = np.array(Q)
                self.G = np.ones((dim_x, len(Q)))
        else:
            self.G = np.array(G)
            self.Q = np.eye(dim_x - self.G.shape[1]) if Q is None else np.array(Q)
        self.R = np.eye(dim_y) if R is None else R

        # 1darray <-> x, P
        self._ind_x = slice(0, dim_x)
        self._ind_P = slice(dim_x, dim_x + dim_x**2)

        # convenience
        self._I = np.eye(dim_x)
        self._log_likelihood = 0.0
    
    def _get_xP(self, vec: np.ndarray):
        x = vec[self._ind_x].reshape((self.dim_x, 1))
        P = vec[self._ind_P].reshape((self.dim_x, self.dim_x))
        return x, P
    
    def _get_1d(self, x, P): return np.concatenate((x, P), axis=None)

    @staticmethod
    def dynamics_fun(x, P, A, Q, b=0, G=1):
        # ẋ = Ax + b
        x = A @ x + b
        # Ṗ = AP + PA' + GQG'
        GQG = np.dot(G, np.dot(Q, np.transpose(G)))
        P = A @ P + P @ A.T + GQG
        return x, P
    
    def measure_fun(self, state) -> np.ndarray:
        return self.H.dot(state)

    def _get_ode(self, Q):
        if Q is None: Q = self.Q
        def f(t, X):
            x, P = self._get_xP(X)
            A = self.A(t) if callable(self.A) else self.A
            b = self.b(t) if callable(self.b) else self.b
            Q = self.Q(t) if callable(self.Q) else self.Q
            x, P = self.dynamics_fun(x, P, A, Q, b, self.G)
            return self._get_1d(x, P)
        return f

    def predict(self, t, Q=None):
        # x and P on the lhs are prior predictions
        # x and P on the rhs are post-measurement estimates

        self.k += 1
        X0 = self._get_1d(self.x, self.P)
        t_range = self.t, t
        X = solve_ivp(self._get_ode(Q), t_range, X0, dense_output=True)
        self.x_prior = lambda tau: self._get_xP(X.sol(tau))[0]
        X1 = X.x[-1]
        self.x, self.P = self._get_xP(X1)
        self.t = t

    def update(self, y, R=None):
        # x and P on the rhs are prior predictions
        # x and P on the lhs are post-measurement estimates

        if R is None: R = self.R
        y = np.array(y).reshape((self.dim_y, 1))
        self.y = y

        # calculate Kalman gain K
        innovation = y - self.measure_fun(self.x)
        PHT = self.P @ self.H.T
        S = self.H @ PHT + R
        S_inv = np.linalg.inv(S)
        self.K = PHT @ S_inv

        # x[k|k] = x[k|k-1] + K(y - Cx[k|k-1])
        self.x = self.x + self.K @ innovation
        
        # P[k|k] = (I-KC)P[k|k-1](I-KC)' + KRK'
        I_KH = self._I - self.K @ self.H
        self.P = I_KH @ self.P @ I_KH.T + self.K @ R @ self.K.T

        # update log(likelihood)
        self._log_likelihood += log_normal_pdf_kf(innovation, S, S_inv)

        # save x, P, K
        if self.save:
            self.x_post[self.k] = np.squeeze(self.x)
            self.P_post[self.k] = self.P
            self.K_post[self.k] = self.K
    
    def step(self, t, y, Q=None, R=None):
        self.predict(t, Q)
        self.update(y, R)

    def _prepare_run(self, x0, P0, t0, N):
        # assert dims
        self.k = 0
        self.t = t0
        self.x = np.reshape(x0, (self.dim_x, 1))
        self.P = P0
        self._log_likelihood = 0.
        if self.save:
            self.x_post = np.nan * np.ones((N, self.dim_x))
            self.P_post = np.nan * np.ones((N, self.dim_x, self.dim_x))
            self.K_post = np.nan * np.ones((N, self.dim_x, self.dim_y))

    def __call__(self, x0, P0, t, y, *args, **kwargs):
        self._prepare_run(x0, P0, t[0], len(t))

        # find x[0|0], P[0|0], K[0] from x[0|-1], P[0|-1]
        self.update(np.reshape(y[0], (self.dim_y, 1)))
        for k in range(1, len(t)): # x[k|k], P[k|k], K[k]
            self.step(t[k], np.reshape(y[k], (self.dim_y, 1)))

        if self.save:
            return StateEstimation(t=t, xhat=self.x_post, y=y, K=self.K_post,
                                   P=self.P_post, success=True)

    def soft_run(self, x0, P0, t, y, *args, **kwargs):
        old = self.save
        self.save = False
        KalmanFilter.__call__(self, x0, P0, t, y, *args, **kwargs)
        self.save = old

    def estimate_Q(self, x0, P0, t, y, Q0_diag=None, bounds=None,
                   call_args=[], call_kw={}, **kwargs):
        q = np.ones(len(self.Q)) if Q0_diag is None else Q0_diag
        if bounds is None:
            bounds = [(0,1)] * len(self.Q)
        def objective_func(Q_diag):
            self.Q = np.diag(Q_diag)
            self.soft_run(x0, P0, t, y, *call_args, **call_kw)
            return -self.log_likelihood
        opt = minimize(objective_func, q, bounds=bounds, **kwargs)
        if opt.success:
            q = opt.x
            print(f"Estimated Q successfully: Q=diag({q})")
        else:
            warnings.warn(f"Q estimation failed, using initial estimate.\n"
                          f"Message: '{opt.message}'")
        self.Q = np.diag(q)
        return self.Q

    @property
    def log_likelihood(self): return self._log_likelihood

    @property
    def likelihood(self): return np.exp(self._log_likelihood)

class ExtendedKalmanFilter(KalmanFilter):
    def __init__(self, dim_x, dim_y, f, h, Jf, Jh, R=None, Q=None, G=None):
        super().__init__(dim_x, dim_y, None, None, b=0, R=R, Q=Q, G=G)
        self.f = f
        self.h = h
        self.Jf = Jf
        self.Jh = Jh
        if isinstance(Jh, np.ndarray):
            self.H = Jh
    
    def dynamics_fun(self, t, x, P, Q, G=1):
        # ẋ = f(t, x)
        x = self.f(t, x)
        # Ṗ = AP + PA' + GQG'
        A = self.Jf(x)
        GQG = np.dot(G, np.dot(Q, np.transpose(G)))
        P = A @ P + P @ A.T + GQG
        return x, P
    
    def measure_fun(self, x) -> np.ndarray:
        if callable(self.Jh):
            self.H = self.Jh(np.squeeze(x))
        return self.h(x)
    
    def _get_ode(self, Q):
        if Q is None: Q = self.Q
        def f(t, X):
            x, P = self._get_xP(X)
            Q = self.Q(t) if callable(self.Q) else self.Q
            x, P = self.dynamics_fun(t, np.squeeze(x), P, Q, self.G)
            return self._get_1d(x, P)
        return f
