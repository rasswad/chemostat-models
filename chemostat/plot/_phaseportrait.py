import numpy as np
import matplotlib as mpl
from scipy.integrate import OdeSolution
from matplotlib import pyplot as plt

from chemostat import utils
from chemostat.growth import *
from chemostat.model import *
from ._nullclines import plot_nullclines


def quiver(x, y, model: BiomodelTI, ax=None, **kwargs):
    u, v = utils.call_ode_xy(x, y, model)
    if not ax:
        ax = plt.gca()
    return ax.quiver(x, y, u, v, angles='xy', **kwargs)

def streamplot(x, y, model: BiomodelTI, ax=None, **kwargs):
    u, v = utils.call_ode_xy(x, y, model)
    if not ax:
        ax = plt.gca()
    kwargs.setdefault('broken_streamlines', False)
    sp = ax.streamplot(x, y, u, v, **kwargs)
    ax.set_xlim(x[0], x[-1])
    ax.set_ylim(y[0], y[-1])
    return sp

def eigenspaces(matrix, origin=[0,0], ax=None, **kwargs):
    A = np.array(matrix)
    pt = np.array(origin)
    assert pt.ndim == 1 and len(pt) == 2, ValueError("'origin' must be of "
                                                     "of length 2")
    assert A.ndim == 2 and A.shape == (2, 2), ValueError("'matrix' must be "
                                                         "a 2x2 array-like")
    w, v = np.linalg.eig(A)
    if ax:
        xmin, xmax, ymin, ymax = ax.axis()
    else:
        ax = plt.gca()
        xmin, xmax, ymin, ymax = -1, 1, -1, 1
    t = max(xmax - xmin, ymax - ymin) / 2
    t = np.linspace(-1.5 * t, 1.5 * t, 3)
    draw = False
    for i in range(len(w)):
        if not np.iscomplex(w[i]):
            kwargs['label'] = f'$\\lambda = {w[i]:.3g}$'
            ax.plot(v[0,i]*t + pt[0], v[1,i]*t + pt[1], **kwargs)
            draw = True
    if draw:
        ax.axis([xmin, xmax, ymin, ymax])
        ax.legend(loc='lower right')
    return ax

def equilibria(points, labels=None, ax=None, **kwargs):
    E = np.array(points).T
    kwargs.setdefault('color', 'black')
    kwargs.setdefault('marker', 'o')
    kwargs.setdefault('linestyle', '')
    if not ax:
        ax = plt.gca()
    ax.plot(E[0], E[1], **kwargs)
    if labels:
        dr = kwargs.pop('space', 0.02)
        size = kwargs.pop('fontsize', 12)
        if 2==E.ndim:
            assert len(labels)==len(points), "'labels' and 'points' lengths do not match"
            for i in range(len(labels)):
                ax.text(E[0,i] + dr, E[1,i] + dr, labels[i], fontsize=size)
        elif 1==E.ndim:
            ax.text(E[0] + dr, E[1] + dr, labels, fontsize=size)
    return ax

def trajectory(sol: OdeSolution, ax=None, arrow_kw={}, **kwargs):
    if not ax:
        ax = plt.gca()
    x, y = sol.x[0], sol.x[1]
    line = ax.plot(x, y, **kwargs)[0]
    # find halfpoint
    diff = np.linalg.norm(np.diff(sol.x), axis=0)
    diff /= np.max(diff)
    ind = np.abs(diff - 0.5).argmin()

    arrow_kw.setdefault('arrowstyle', '-|>')
    arrow_kw.setdefault('transform', ax.transData)
    arrow_kw.setdefault('mutation_scale', 10 * arrow_kw.pop('arrowsize', 1))
    arrow_kw.setdefault('color', line.get_color())
    arrow_kw.setdefault('zorder', line.get_zorder() + 1)
    p = mpl.patches.FancyArrowPatch((x[ind], y[ind]), (x[ind+1], y[ind+1]), **arrow_kw)
    ax.add_patch(p)
    return ax

class PhasePortrait(object):
    def __init__(self, model: BiomodelTI, xlim=None, ylim=None,
                 quiver_mesh=10, stream_mesh=100, mesh_mode='num', ax=None):
        assert 2==len(model), "'model' is not 2-dimensional"
        self.model = model
        self.setlims(xlim, ylim)
        self._qmesh = quiver_mesh
        self._smesh = stream_mesh
        self._mode = mesh_mode
        self.ax = ax if ax else plt.gca()

    def plot(self, quiver_kw={}, stream_kw={}):
        self.ax.cla()
        self.quiver(**quiver_kw)
        self.streamplot(**stream_kw)
        self.ax.set_xlim(self.xlim)
        self.ax.set_ylim(self.ylim)
        return self
    
    def quiver(self, x=None, y=None, **kwargs):
        x, y, kwargs = self.__preprocess(x, y, kwargs)
        ax = kwargs.pop('ax', self.ax)
        kwargs.setdefault('alpha', 0.2)
        kwargs.setdefault('scale_units', 'xy')
        self.Q = quiver(x, y, self.model, ax=ax, **kwargs)
        return self.Q
    
    def streamplot(self, x=None, y=None, **kwargs):
        x, y, kwargs = self.__preprocess(x, y, kwargs)
        ax = kwargs.pop('ax', self.ax)
        kwargs.setdefault('density', (0.3, 0.6))
        self.SP = streamplot(x, y, self.model, ax=ax, **kwargs)
        return self.SP
    
    def setlims(self, xlim=None, ylim=None):
        if not xlim or not ylim:
            E = self.model.equilibria().T
            xeq = np.max(E[0])
            yeq = np.max(E[1])
            if xeq > 0:
                xmax = 1.5 * xeq
                ymax = 3 * yeq if yeq > 0 else xmax
            elif yeq > 0:
                ymax = 2 * yeq
                xmax = ymax
            else:
                if hasattr(self.model.growth, 'inverse') and hasattr(self.model, 'D'):
                    val = self.model.growth.inverse(self.model.D)
                    xmax = ymax = 2 * val
                elif hasattr(self.model.growth, 'preimages') and hasattr(self.model, 'D'):
                    val = max(self.model.growth.preimages(self.model.D))
                    xmax = ymax = 2 * val
                else:
                    xmax = ymax = 1
            if not (xlim or ylim):
                xlim = (0, xmax)
                ylim = (0, ymax)
            if not xlim:
                xlim = (0, xmax) if not hasattr(self, 'xlim') else self.xlim
            if not ylim:
                ylim = (0, ymax) if not hasattr(self, 'ylim') else self.ylim
        else:
            if not isinstance(xlim, tuple) or not isinstance(ylim, tuple) or 2!=len(xlim) or 2!=len(ylim):
                raise ValueError("'xlim' and 'ylim' must be tuples of size 2")
            if xlim[0] >= xlim[1] or ylim[0] >= ylim[1]:
                raise ValueError("'xlim' and 'ylim' must be in increasing order")
        self.xlim, self.ylim = xlim, ylim
        return xlim, ylim
    
    def __preprocess(self, x, y, kwargs):
        if not x or not y:
            mode = kwargs.pop('mode', self._mode)
            mesh = kwargs.pop('mesh', self._qmesh)
            xx, yy = self.__getmesh(mesh, mode)
            if not x: x = xx
            if not y: y = yy
        return x, y, kwargs

    def __getmesh(self, mesh, mode: str):
        assert mode.lower() in ['num', 'step'], "'mode' must be 'num' or 'step'"
        func = np.linspace if mode=='num' else np.arange
        x = func(self.xlim[0], self.xlim[1], mesh)
        y = func(self.ylim[0], self.ylim[1], mesh)
        return x, y

    def set_xlabel(self, xlabel: str, **kwargs):
        return self.ax.set_xlabel(xlabel, **kwargs)
    
    def set_ylabel(self, ylabel: str, **kwargs):
        return self.ax.set_ylabel(ylabel, **kwargs)
    
    def set_axis_labels(self, xlabel: str="", ylabel: str="", **kwargs):
        self.set_xlabel(xlabel, **kwargs)
        self.set_ylabel(ylabel, **kwargs)
        return self


class PhasePortraitExtended(PhasePortrait):
    def __init__(
            self, model: BiomodelTI, xlim=None, ylim=None, dlim=None,
            quiver_mesh=10, stream_mesh=100, mesh_mode='num',
            growth_plot=True, equilibria_portraits=True,
            width=10, height=8, h_ratio=[1, 5], w_ratio=[2, 1],
            hspace=0.2, wspace=None):

        f = plt.figure(figsize=(width, height))

        # find rows and cols
        self.E = model.equilibria()
        N = len(self.E) if self.E.ndim == 2 else 1
        rows = max(2, N)
        cols = 1 + equilibria_portraits
        if growth_plot:
            h_ratio = [h_ratio[0]] + [h_ratio[1] / rows] * rows
            rows += 1
        else:
            h_ratio = [1] * rows
        gs = plt.GridSpec(rows, cols, height_ratios=h_ratio,
                          width_ratios=w_ratio[:cols])
        
        # add axes
        self.ax_pp = f.add_subplot(gs[int(growth_plot):,0])
        if growth_plot:
            self.ax_growth = f.add_subplot(gs[0,0], sharex=self.ax_pp)
            self.ax_growth.tick_params(labelbottom=False)
        if equilibria_portraits:
            r1 = rows - 1
            self.ax_eq = [f.add_subplot(gs[i,1]) for i in range(r1, r1-N, -1)]
        
        PhasePortrait.__init__(self, model, xlim=xlim, ylim=ylim, ax=self.ax_pp,
                               quiver_mesh=quiver_mesh, stream_mesh=stream_mesh,
                               mesh_mode=mesh_mode)
        self.setlims(xlim, ylim, dlim)
        self.gs = gs
        self.lines = []
        f.subplots_adjust(hspace=hspace, wspace=wspace)
        self._figure = f

    def setlims(self, xlim=None, ylim=None, dlim=None):
        if dlim:
            # set dlim from tuple only
            if not isinstance(dlim, tuple) or 2!=len(dlim):
                raise ValueError("'dlim' must be a tuple of size 2")
            if dlim[0] >= dlim[1]:
                raise ValueError("'dlim' must be in increasing order")
            self.dlim = dlim
            if not (hasattr(self, 'xlim') and hasattr(self, 'ylim')):
                super().setlims(xlim, ylim)
        else:
            super().setlims(xlim, ylim)
            if not ((xlim or ylim) and hasattr(self, 'dlim')):
                # set dlim automatically
                sval = np.linspace(self.xlim[0], self.xlim[1], 3)
                if hasattr(self.model.growth, 'max'):
                    self.dlim = 0, 1.5 * self.model.growth.max
                else:
                    try:
                        self.dlim = 0, 1.5 * np.max(self.model.growth(sval))
                    except NotImplementedError:
                        self.dlim = 0, 1.5 * utils.__get_dmax(self.ax_growth)
        return self.xlim, self.ylim, self.dlim
    
    def plotgrowth(self, x=None, y=None, **kwargs):
        try:
            if not x:
                x = np.linspace(self.xlim[0], self.xlim[1], 100)
            y = self.model.growth(x)
        except NotImplementedError:
            if not y:
                dmax = utils.__get_dmax(self.model.growth)
                y = np.linspace(0, dmax, 100)
            x = self.model.growth.inverse(y)
        ax = kwargs.pop('ax', self.ax_growth)
        dlim = kwargs.pop('dlim', self.dlim)
        self.lines += ax.plot(x, y, **kwargs)
        ax.set_ylim(dlim)
        return self.lines
    
    def showpointcoord(self, s, d, **kwargs):
        kwargs.setdefault('color', 'black')
        kwargs.setdefault('linestyle', '--')

        if s != np.nan:
            self.lines += self.ax_pp.axvline(x=s, ymin=0, ymax=1, **kwargs),
            self.lines += self.ax_growth.plot([s,s], [0,d], **kwargs)
            self.lines += self.ax_growth.plot([0,s], [d,d], **kwargs)
        else:
            self.lines += self.ax_growth.axhline(y=d, xmin=0, xmax=1, **kwargs),
        return self.lines
    
    def showdilution(self, D=None, **kwargs):
        if D is None:
            if hasattr(self.model, 'D'):
                D = self.model.D
            else:
                raise ValueError("'D' has to be set since not defined in model")
        if hasattr(self.model.growth, 'preimages'):
            ss = self.model.growth.preimages(D)
            for s in ss:
                self.showpointcoord(s, D, **kwargs)
        else:
            try:
                s = self.model.growth.inverse(D)
            except NotImplementedError:
                s = np.nan
            self.showpointcoord(s, D, **kwargs)
        return self.lines
    
    def show_s_inflow(self, Sin=None, **kwargs):
        if Sin is None:
            if hasattr(self.model, 'Sin'):
                Sin = self.model.Sin
            else:
                raise ValueError("'Sin' has to be set since not defined in model")
        try:
            d = self.model.growth(Sin)
        except NotImplementedError:
            # TODO: find 'd' from inverse (dichotomy for example)
            #raise NotImplementedError
            inv = lambda s: self.model.growth.inverse(s)
            dmax = self.dlim[1]
            try:
                d = utils.preimage_of_monotonic(inv, Sin, 0, dmax)
            except ValueError:
                dmax = utils.__get_dmax(self.model.growth, 0.01)
                d = utils.preimage_of_monotonic(inv, Sin, 0, dmax)
        return self.showpointcoord(Sin, d, **kwargs)
    
    def eq_phaseportrait(self, zoom=5, eigen=True, **kwargs):
        E = self.E
        dx = (self.xlim[1] - self.xlim[0]) / zoom
        dy = (self.ylim[1] - self.ylim[0]) / zoom
        kwargs.setdefault('density', (0.3, 0.6))
        self.sp_eq = []
        for i in range(len(E)):
            pt = E[i]
            x = np.linspace(pt[0] - dx, pt[0] + dx)
            y = np.linspace(pt[1] - dy, pt[1] + dy)
            self.sp_eq.append(streamplot(x, y, self.model, ax=self.ax_eq[i], **kwargs))
            if eigen:
                try:
                    J = self.model.jacobian(pt)
                    eigenspaces(J, pt, ax=self.ax_eq[i])
                except NotImplementedError:
                    pass
        return self

    def showequilibria(self, labels='auto', **kwargs):
        if isinstance(labels, str) and labels.lower() == 'auto':
            N = len(self.E) if self.E.ndim == 2 else 1
            labels = [f"$E_{i}$" for i in range(len(self.E))]
        equilibria(self.E, labels, self.ax_pp, **kwargs)
        if hasattr(self, 'ax_eq'):
            for i in range(len(self.E)):
                equilibria(self.E[i], labels[i], self.ax_eq[i], **kwargs)
    
    def shownullclines(self, n_pts=100):
        self.null = plot_nullclines(self.model, ax=self.ax_pp)
        if self.null: self.ax_pp.legend(loc='upper right')

    def plot(self, point_lines=True, eigen=True,
             quiver_kw={}, stream_kw={}, growth_kw={},
             eq_kw={}, pt_lines_kw={}, **kwargs):
        super().plot(quiver_kw, stream_kw)
        if hasattr(self, 'ax_growth') and kwargs.pop('growth', True):
            self.ax_growth.cla()
            self.plotgrowth(**growth_kw)
            if point_lines:
                # if D in kwargs, use it, else try model.D
                D = self.model.D if hasattr(self.model, 'D') else None
                D = kwargs.pop('D', D)
                D and self.showdilution(D, **pt_lines_kw)
                Sin = self.model.Sin if hasattr(self.model, 'Sin') else None
                Sin = kwargs.pop('Sin', Sin)
                Sin and self.show_s_inflow(Sin, **pt_lines_kw)
        if hasattr(self, 'ax_eq'):
            zoom = kwargs.pop('eq_zoom', 5)
            for ax in self.ax_eq: ax.cla()
            self.eq_phaseportrait(zoom, eigen=eigen, **eq_kw)
        self.shownullclines()
        eq_labels = kwargs.pop('eq_labels', 'auto')
        self.showequilibria(eq_labels, **kwargs)
        plt.sca(self.ax_pp)
        return self
    
    def set_ylabel_growth(self, ylabel: str, **kwargs):
        return self.ax_growth.set_ylabel(ylabel, **kwargs)
    
    def set_axis_labels(self, xlabel: str="", ylabel: str="",
                        rlabel: str="", **kwargs):
        self.set_xlabel(xlabel, **kwargs)
        self.set_ylabel(ylabel, **kwargs)
        self.set_ylabel_growth(rlabel, **kwargs)
        return self
    
    @property
    def figure(self):
        """Access the :class:`matplotlib.figure.Figure` object underlying the grid."""
        return self._figure
    
    def suptitle(self, title, **kwargs):
        return self.figure.suptitle(title, **kwargs)


def phaseportrait(model: BiomodelTI, xlim=None, ylim=None, ax=None, **kwargs):
    pp_keys = {'quiver_mesh', 'stream_mesh', 'mesh_mode'}
    pp_keys = set(kwargs.keys()).intersection(pp_keys)
    pp_kw = {}
    for key in pp_keys:
        pp_kw[key] = kwargs.pop(key)
    return PhasePortrait(model, xlim, ylim, ax=ax, **pp_kw).plot(**kwargs)

def extendedphaseportrait(
        model: BiomodelTI, xlim=None, ylim=None, dlim=None, **kwargs):
    pp_keys = {'quiver_mesh', 'stream_mesh', 'mesh_mode',
               'growth_plot', 'equilibria_portraits',
               'width', 'height', 'h_ratio', 'w_ratio', 'hspace', 'wspace'}
    pp_keys = set(kwargs.keys()).intersection(pp_keys)
    pp_kw = {}
    for key in pp_keys:
        pp_kw[key] = kwargs.pop(key)
    pp = PhasePortraitExtended(model, xlim, ylim, dlim, **pp_kw)
    return pp.plot(**kwargs)

