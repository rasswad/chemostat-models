from abc import ABC, abstractmethod
from copy import deepcopy
from typing import Any

import numpy as np
from scipy.integrate import solve_ivp
from scipy.interpolate import interp1d
from scipy.linalg import expm

from chemostat import utils
from chemostat.utils import nonnegativeode
from chemostat.growth import *


class Trajectory(utils.OptimizeResult):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        val = self.x if 'x' in self else self.y
        self.interp = interp1d(self.t, val, kind='previous',
                               fill_value='extrapolate')
    
    def __call__(self, t):
        return self.interp(t)


class Biomodel(ABC):
    """Base class for biology models."""
    
    @abstractmethod
    def ode(self, t, state):
        """The state equation of the model.
        ẋ = f(t, x)
 
        Parameters
        ----------
        t : {number, array-like}
            Time.
        state : array-like of shape (dim,) or (dim,...)
            State vector x(t) of the model at the given time.
        
        
        Returns
        -------
        state_dot : ndarray with same shape as state
            State time derivative ẋ(t).
        """

    @abstractmethod
    def equilibria(self):
        """Calculates the equilibria points of the model,
        i.e. points in the state space such that the state equation is zero.

        Returns
        -------
        E : ndarray of shape (nb_equilibria, dim)
            A matrix whose rows are equilibria coordinates.
        """

    @abstractmethod
    def jacobian(self, state):
        """The jacobian matrix of the model.

        Parameters
        ----------
        t : {number, array-like}
            Time.
        state : array-like of shape (dim,) or (dim,...)
            State vector x(t) of the model at the given time.
        """

    def calculate_trajectory(self, initial_state, time_span, **options):
        """Calculates the state trajectory over time.
        Solves an initial value problem for the model state equation.
        This function calls :meth:`scipy.integrate.solve_ivp` that numerically \
        integrates a system of ordinary differential equations given an \
        initial value.

        Parameters
        ----------
        initial_state : array-like of shape (dim,)
            Initial state.
        time_span : 2-member sequence of floats
            Interval of integration [t0, tf].
        **options : keyword arguments
            Options passed to :meth:`scipy.integrate.solve_ivp`.
        
        Returns
        -------
        result : `OdeResult` object
            A bunch object.
        """
        options.setdefault("vectorized", True)
        sol = solve_ivp(self._get_ode(1), time_span, initial_state, **options)
        return Trajectory(t=sol.t, x=sol.y, status=sol.status,
                          success=sol.success)

    @abstractmethod
    def __init__(self):
        self._dim = 0
        self._vars = []
        self._abbrv = {}
        self.growth: GrowthRate = None
    
    def __getattr__(self, name: str):
        if name.startswith('_'):
            return self.__getattribute__(name)
        if name in self._abbrv.keys():
            return self.__getattribute__(self._abbrv[name])
        return self.__getattribute__(name)
    
    def __setattr__(self, name: str, value):
        if name.startswith('_'):
            super().__setattr__(name, value)
        elif name in self._abbrv.keys():
            super().__setattr__(self._abbrv[name], value)
        else:
            super().__setattr__(name, value)
    
    def __str__(self) -> str:
        s = f"{type(self).__name__}("
        s += f"dimension={len(self)}"
        if self._vars:
            s += f", state=[{', '.join(self._vars)}]"
        s += ")"
        return s

    def __repr__(self) -> str:
        d = vars(self).copy()
        for k in vars(self).keys():
            if k.startswith('_'): d.pop(k)
        order_keys = list(d.keys())
        def key(item):
            try:
                return order_keys.index(item[0].lower())
            except ValueError:  # item not in list
                return np.inf
        s = str(self) + '\n'
        s += utils._dict_formatter(d, sorter=lambda d: sorted(d.items(), key=key))
        return s

    def __len__(self): return self._dim
    def __getitem__(self, key): return self._vars.__getitem__(key)
    def __setitem__(self, key, value: str):
        return self._vars.__setitem__(key, value)
    
    def _checkdim(self, state):
        if len(self) != len(state):
            raise ValueError(f"expected {self._dim} dimensions for 'state', got {len(state)}")
    
    def set_vars(self, *args):
        """Sets state variable symbols/names."""
        if 0==len(args):
            self._vars = [f'x{k}' for k in range(1, self._dim + 1)]
        elif 1==len(args) and isinstance(args[0], list):
            self._checkdim(args[0])
            self._vars = args[0]
        else:
            self._checkdim(args)
            self._vars = [arg for arg in args]

    def _get_ode(self, time):
        if time not in [1, 2]:
            raise ValueError("'time' has to be in [1, 2]")
        if 1==time:
            return lambda t, x: self.ode(t, x)
        return lambda x, t: self.ode(t, x)


class BiomodelTI(Biomodel):
    """Base class for time-invariant biology models."""

    @abstractmethod
    def ode(self, state):
        """The state equation of the time-invariant model.
        ẋ = f(x)
 
        Parameters
        ----------
        state : array-like of shape (dim,) or (dim,...)
            State vector x(t) of the model at the given time.
        
        Returns
        -------
        state_dot : ndarray with same shape as state
            State time derivative ẋ(t).
        """
        self._checkdim(state)
        if not isinstance(state, np.ndarray):
            state = np.array(state)
        return np.zeros(state.shape)
    
    def calculate_trajectory(self, initial_state, time_span, **options):
        sol = solve_ivp(self._get_ode(1), time_span, initial_state, **options)
        return Trajectory(t=sol.t, x=sol.y, status=sol.status,
                          success=sol.success)

    def linearize(self, state):
        """Linearizes the model around a given state.
        ẋ = f(x)
        Returns a linear time-invariant model,
        that is the first-order Taylor expansion at the given state x0
        ż = f(x0) + J(x0)(z - x0)
          = J(x0)z + f(x0) - J(x0)x0
          = Az + b
 
        Parameters
        ----------
        state : array-like of shape (dim,) or (dim,...)
            State vector x0 to linearize around.
        
        Returns
        -------
        linear_model : :class:`chemostat.model.BiomodelLTI`
            Linear time-invariant model.
        """
        self._checkdim(state)
        J = self.jacobian(state)
        b = self.ode(state) - np.matmul(J, state)
        return BiomodelLTI(J, b)
    
    def _get_ode(self, time):
        if time not in [0, 1, 2]:
            raise ValueError("'time' has to be in [0, 1, 2]")
        if 0==time:
            return lambda x: self.ode(x)
        if 1==time:
            return lambda t, x: self.ode(x)
        return lambda x, t: self.ode(x)
        

class BiomodelLTI(BiomodelTI):
    """Base class for linear time-invariant biology models."""
    def __init__(self, A: np.ndarray, b: np.ndarray):
        if 2 != A.ndim or A.shape[0] != A.shape[1]:
            raise ValueError(f"'A' must be a 2D square numpy.ndarray, got {A.shape}")
        if 1 != b.ndim:
            raise ValueError(f"'b' must be a 1D numpy.ndarray, got {b.shape}")
        if len(A) != len(b):
            raise ValueError("_dimensions of 'A' and 'b' don't match")
        self._dim = len(A)
        self.set_vars()
        self.A = A
        self.b = b
    
    def ode(self, state):
        self._checkdim(state)
        Ax = np.matmul(self.A, state)
        if utils.iszero(self.b):
            return Ax
        if 1 == Ax.ndim:
            return Ax + self.b
        if 2 == Ax.ndim:
            B = np.matmul(self.b.reshape((len(self), 1)), np.ones((1, Ax.shape[1])))
            return Ax + B
        raise ValueError(f"expected {len(self)} _dimensions for 'state', got {len(state)}")
    
    def jacobian(self): return self.A
    
    def equilibria(self):
        if utils.iszero(self.b):
            return np.zeros(len(self))
        return np.linalg.solve(self.A, -self.b)

    def calculate_trajectory(self, initial_state, time_span, **options):
        # to-do: calculate using exponential instead
        return super().calculate_trajectory(initial_state, time_span, **options)
    
    def linearize(self, state):
        raise NotImplementedError("'BiomodelLTI' is linear")
    

class ReducedModelTI(BiomodelTI):
    """Base class for reduced time-invariant models.
    The model is reduced from a higher dimensional model
    by exploiting a conservation law which is usually derived
    from the left kernel of the stochiometric matrix of the model.
    """

    def __init__(self, base_model: BiomodelTI, drop_dim,
                 conservation_law: callable):
        super().__init__()
        self.base = base_model
        if isinstance(drop_dim, int):
            if drop_dim < 0 or drop_dim >= base_model._dim:
                raise ValueError(f"'drop_dim' be a positive integer less "
                                 "than {base_model._dim}")
            self.drop_dim = drop_dim
        elif isinstance(drop_dim, str):
            try:
                self.drop_dim = base_model._vars.index(drop_dim)
            except ValueError:
                raise ValueError(f"'drop_dim' not found in {base_model._vars}")
        else:
            raise ValueError
        self._dim = base_model._dim - 1
        self._vars = deepcopy(base_model._vars)
        self._vars.pop(self.drop_dim)
        self.drop_func = conservation_law
        self.growth = self.base.growth
    
    def __getattr__(self, name: str):
        return self.base.__getattribute__(name)
    
    def ode(self, state):
        self._checkdim(state)
        value = self.drop_func(state)
        full_state = np.insert(state, self.drop_dim, value, axis=0)
        dot = self.base.ode(full_state)
        return np.delete(dot, self.drop_dim, axis=0)
    
    def jacobian(self, state): raise NotImplementedError
    
    def equilibria(self):
        return np.delete(self.base.equilibria(), self.drop_dim, axis=-1)

