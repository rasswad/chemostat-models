import numpy as np

from chemostat.growth import *
from chemostat.utils import nonnegativeode
from ._base import BiomodelTI
from ._classical import ConstantYield, VariableYield


class FluoCstYield(ConstantYield):
    def __init__(self, growth: GrowthRate, dilution_rate, nutrient_feed,
                 yield_, fluo_ratio):
        BiomodelTI.__init__(self)
        self._abbrv = dict(
            mu='growth',
            D='dilution_rate',
            Sin='nutrient_feed',
            Y='yield_',
            a='fluo_ratio',
            N='stoichiometric_matrix'
        )
        self._dim = 3
        self.set_vars('s', 'x', 'f')
        self.mu = growth
        self.D = dilution_rate
        self.Sin = nutrient_feed
        self.Y = yield_
        self.a = fluo_ratio
        self.N = np.array([-1/yield_, 1 - fluo_ratio, fluo_ratio])

    def ode(self, state):
        dot = BiomodelTI.ode(self, state)
        if isinstance(self.mu, GrowthRateSimple):
            mu = self.mu(state[0])
        elif isinstance(self.mu, GrowthRateSX):
            mu = self.mu(state[0], state[1])
        else:
            raise ValueError("unknown or incompatible GrowthRate")
        mu_x = mu * state[1]
        a_mu_x = self.a * mu_x
        dot[0] = self.D * (self.Sin - state[0]) - (mu_x / self.Y)
        dot[1] = (mu_x - a_mu_x) - self.D * state[1]
        dot[2] = a_mu_x - self.D * state[2]
        return dot

    def equilibria(self):
        raise NotImplementedError

    def jacobian(self, state):
        #self._checkdim(state)
        J = np.zeros((len(self), len(self)))
        J[:2, :2] = ConstantYield.jacobian(self, state[:2])
        a_dmu_x = self.a * J[1, 0]
        a_mu = self.a * J[1,1] + self.D
        J[1, 0] -= a_dmu_x
        J[1, 1] -= a_mu
        J[2, :] = [a_dmu_x, a_mu, -self.D]
        return J


class ABModel(BiomodelTI):
    def __init__(self, e_growth: Monod, vitamin_uptake: Monod,
                 c_growth_velocity: ShiftedMonod,
                 dilution_rate, nutrient_feed, nutrient_yield,
                 vitamin_production, quota='total'):
        super().__init__()
        self._abbrv = dict(
            rho='vitamin_uptake',
            mu_e='e_growth',
            mu_c='c_growth_velocity',
            phi='c_growth',
            psi='growth',
            D='dilution_rate',
            Sin='nutrient_feed',
            Y='nutrient_yield',
            beta='vitamin_production',
        )
        self._dim = 5
        self.D = dilution_rate
        self.Sin = nutrient_feed
        self.Y = nutrient_yield
        self.beta = vitamin_production
        self.mu_e = e_growth
        self.mu_c = c_growth_velocity
        self.rho = vitamin_uptake
        self.phi = VarYieldMonod(self.rho, self.mu_c)
        self.psi = ABGrowthRate(self.mu_e, self.phi, self.Y, self.beta)
        self.quota = quota
        self.e_model = ConstantYield(e_growth, dilution_rate, nutrient_feed, nutrient_yield)
        self.e_model.set_vars(self._vars[:2])
        e_eq = self.e_model.equilibria()
        Vin_eq = self.Vin(e_eq[1, 0], e_eq[1, 1]) if 2 == len(e_eq) else 0
        self.c_model = VariableYield(
            vitamin_uptake, c_growth_velocity, dilution_rate, Vin_eq, quota)
        self.c_model.set_vars(self._vars[2:])

    @property
    def quota(self):
        return self.__quota

    @quota.setter
    def quota(self, value: str):
        value = value.lower()
        if value not in ['total', 'cell']:
            raise ValueError("'quota' has to be 'total' (default) or 'cell'")
        self.__quota = value
        var = 'q' if value == 'cell' else 'z'
        default = ['s', 'e', 'v', 'c', var]
        new_vars = self._vars[:4] + [var] if self._vars else default
        self.set_vars(new_vars)

    def Vin(self, s, e):
        return self.Y * self.mu_e(s) * e / self.D

    @nonnegativeode
    def ode(self, state):
        dot = super().ode(state)
        mue_e = self.mu_e(state[0]) * state[1]
        rho = self.rho(state[2])
        rho_c = rho * state[3]
        with np.errstate(divide='ignore', invalid='ignore'):
            muc = self.mu_c(state[4]) if self.quota == 'cell' else self.mu_c(state[4] / state[3])
        dot[0] = self.D * (self.Sin - state[0]) - mue_e
        dot[1] = mue_e - self.D * state[1]
        dot[2] = self.Y * mue_e - self.D * state[2] - rho_c
        dot[3] = (muc - self.D) * state[3]
        if self.quota == 'total':
            dot[4] = rho_c - self.D * state[4]
        else:
            dot[4] = rho - muc * state[4]
        return dot

    def equilibria(self):
        E = [[self.Sin, 0, 0, 0, 0]]
        B = self.e_model.equilibria()
        if 2 == len(B):
            self.c_model.quota = self.quota
            A = self.c_model.equilibria()
            E.append([0] * 5)
            E[1][:2] = B[1, :]
            E[1][2:] = A[0, :]
            if 2 == len(A):
                E.append([0] * 5)
                E[2][:2] = B[1, :]
                E[2][2:] = A[1, :]
        return np.array(E)

    def jacobian(self, state):
        raise NotImplementedError
