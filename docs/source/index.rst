.. Chemostat documentation master file, created by
   sphinx-quickstart on Tue Apr 11 20:16:49 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Chemostat's documentation!
=====================================

.. toctree::

   api

..
   autogenerated: toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
