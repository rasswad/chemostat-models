# Chemostat

A toolbox for simulating dynamical models of growth of microorganisms.
This package implements my work throughout my PhD these models, and will
eventually feature `observer` and `control` modules for the studied biomodels. 

## Setup and usage

### Setting up a virtual environment

First off, install an environment with your preferred utility (venv or conda).
Navigate to the repository root directory.

To setup a virtual environment with the python `venv` and `pip` utilities, run:

```sh
python -m venv .chemostat-env
source .chemostat-env/bin/activate
pip install -r requirements.txt
```

To setup a conda environment, run:

```sh
conda env create --file=environment.yml
conda activate chemostat-env
```

### Usage

Explore and run any script from the `examples` directory.